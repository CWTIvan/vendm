<?php

$action = strtolower($_REQUEST["action"]); //текущее действие


$db_auth = [];
include_once('_auth.php'); //Данные для авторизации

function class_autoloader($class) {
    include 'classes/'.$class.'.php';
}
spl_autoload_register('class_autoloader');


//Класс для работы с БД
$db = new db($db_auth);



//============================================
//Получаем исходные данные задачи в формате JSON
if ($action === 'get_initial_data')
{
    $res = $db->GetInitialData();
    print_r($res);
}


//============================================
//Заносим монетку в автомат
if ($action === 'put_coin')
{
    $coin = intval($_REQUEST["coin"]);
    $res = $db->put_coin($coin);
    $response = ["status" => $res];
    echo json_encode($response);
}



//============================================
//Купить что-то из товара
if ($action === 'buy')
{
    $code = intval($_REQUEST["code"]);
    $res = $db->buy($code);
    $response = ["status" => $res];
    echo json_encode($response);
}



//============================================
//Забрать сдачу
if ($action === 'take_change')
{
    $res = $db->take_change();
    $response = ["status" => $res];
    echo json_encode($response);
}



//============================================
//Попробовать купить что-то из находящегося в автомате
if ($action === 'reset')
{
    $res = $db->reset();
    $response = ["status" => $res];
    echo json_encode($response);
}


