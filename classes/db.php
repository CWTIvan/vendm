<?php

/**
 * Класс для подключения к базе данных
 */

class db
{

    protected $host = ''; // адрес сервера
    protected $database = ''; // имя базы данных
    protected $user = ''; // имя пользователя
    protected $password = ''; // пароль

    protected $link; //Ссылка на подключение


    //Конструктор
    public function __construct(array $credits)
    {
        $this->connect($credits);
    }


    //Подсоединяемся к БД
    public function connect(array $credits): void
    {
        if (isset($credits))
        {
            $this->setCredits($credits);
        }
        // подключаемся к серверу
        $link = mysqli_connect($this->host, $this->user, $this->password, $this->database)
            or die ('Ошибка подключения. '. mysqli_error($link));
        $this->link = $link;
    }


    /*
     * Устанавливаем данные для подключения к БД
     *
     */
    public function setCredits(array $credits): void
    {
        foreach ($credits as $credit_key =>$credit_value)
        {
            $this->$credit_key = $credit_value;
        }
    }

    /*
     * Запрос к БД
     *
     */
    public function query(string $query)
    {
        return mysqli_query($this->link, $query);
    }


    //Деструктор
    public function __destruct()
    {
        if (isset($this->link)) $this->link->close();
    }


    //Получаем массив данных на выходе
    public function getArray($table, $params = '')
    {
        $res = $this->query('SELECT * FROM '.$table. ' '.$params);
        $arr = [];
        if ($res)
        {
            while ($row = $res->fetch_array(MYSQLI_ASSOC))
            {
                $arr[] = $row;
            }
            $res->free();
        }
        return $arr;
    }



    //Получаем исходные данные из БД
    public function GetInitialData()
    {
        //Получаем исходные данные из базы
        $vm_coins = $this->getArray('vm_coins');
        $vm_items = $this->getArray('vm_items');
        $user_items = $this->getArray('user_items');
        $users_session = $this->getArray('users_session');
        $items_db = $this->getArray('items');
        $users = $this->getArray('users');

        //Если пользователь уже что-то купил, то выводим это здесь
        $user_items_captions = ["user_items" => []];
        if (isset($user_items))
        {
            foreach ($user_items as $key => $item)
            {
                $cur_name = '';
                foreach ($items_db as $item_key => $item_val)
                {
                    if ($item_val['code'] === $item["code"])
                    {
                        $cur_name = $item_val["caption"];
                    }
                }
                $user_items_captions["user_items"][] = $cur_name;
            }
        }


        //Пробегаемся по результатам и выдаем по коду - название товара в vm
        foreach ($vm_items as $key => &$item)
        {
            $cur_name = '';
            foreach ($items_db as $item_key => $item_val)
            {
                if ($item_val['code'] === $item["code"])
                {
                    $cur_name = $item_val["caption"];
                    $item["name"] = $cur_name;
                }
            }
        }

        $vm_items_res = ["vm_items" => $vm_items]; //чтобы API корректно отрабатывал
        $users_session_res = ["money_inside_vm" => $users_session[0]["money_inside_vm"]];

        //Возвращаем данные состояния приложения, пользователя и аппарата
        $result = array_merge($users[0], $vm_coins[0], $user_items_captions, $vm_items_res, $users_session_res);
        return json_encode($result);

    }


    //Кладем монетку в автомат
    public function put_coin($coin)
    {
        //Узнаем, сколько у пользователя осталось моент данного достоинства
        $res = $this->getArray('users');
        $coin_remains = $res[0]['user_coin_'.$coin];
        if ($coin_remains <= 0)
        {
            return 'not_enough_coins';
        }

        //Устанавливаем соответствие между монетами и полями БД
        switch ($coin) {
            case 1:
                $db_field_users = 'user_coin_1';
                $db_field_vm_coins = 'vm_coin_1';
                break;
            case 2:
                $db_field_users = 'user_coin_2';
                $db_field_vm_coins = 'vm_coin_2';
                break;
            case 5:
                $db_field_users = 'user_coin_5';
                $db_field_vm_coins = 'vm_coin_5';
                break;
            case 10:
                $db_field_users = 'user_coin_10';
                $db_field_vm_coins = 'vm_coin_10';
                break;
            default:
                $db_field_users = 'user_coin_1';
                $db_field_vm_coins = 'vm_coin_1';
        }
        $query_users = 'UPDATE users SET users.'.$db_field_users.' = users.'.$db_field_users.' - 1 WHERE users.id=1';
        $query_vm_coins = 'UPDATE vm_coins SET vm_coins.'.$db_field_vm_coins.' = vm_coins.'.$db_field_vm_coins.' + 1 WHERE vm_coins.id=1';
        $query_user_session = 'UPDATE users_session SET users_session.money_inside_vm = users_session.money_inside_vm + '.$coin.' WHERE users_session.user_id=1';
        $this->query('set autocommit=0');
        $this->query('Start transaction');
        $this->query($query_users);
        $this->query($query_vm_coins);
        $this->query($query_user_session);
        $this->query('commit');
        return 'success';
    }




    //Покупаем что-то в автомате
    public function buy($code)
    {
        //Узнаем, сколько внесено денег
        $res = $this->getArray('users_session', 'WHERE users_session.user_id=1');
        $money_inside = $res[0]['money_inside_vm'];

        //Узнаем ещё раз стоимость товара, который хотим купить и вообще остался ли он на ветрине
        $res = $this->getArray('vm_items', 'WHERE code='.$code);
        $price = $res[0]['price'];
        $remains = $res[0]['qnt'];
        if ($remains <= 0) return 'not_enough_items';

        //Если у нас хватает денег
        if ($price <= $money_inside)
        {
            $query_vm_items_qnt = 'UPDATE vm_items SET vm_items.qnt = vm_items.qnt - 1 WHERE vm_items.code='.$code;
            $query_user_items_add = 'INSERT INTO `user_items` (`id`, `user_id`, `code`) VALUES (NULL, "1", "'.$code.'");';
            $query_user_session = 'UPDATE users_session SET users_session.money_inside_vm = users_session.money_inside_vm - '.$price.' WHERE users_session.user_id=1';
            $this->query('set autocommit=0');
            $this->query('Start transaction');
            $this->query($query_vm_items_qnt);
            $this->query($query_user_items_add);
            $this->query($query_user_session);
            $this->query('commit');
            return 'success';
        }
        else
        {
            return 'not_enough_money';
        }
    }



    //сбрасываем сессию (вводим первоначальные данные)
    public function reset()
    {
        $query_users = 'UPDATE users SET users.user_coin_1=10,
                                         users.user_coin_2=30,
                                         users.user_coin_5=20,
                                         users.user_coin_10=15
                                         WHERE users.id=1';
        $query_vm_coins = 'UPDATE vm_coins SET vm_coins.vm_coin_1 = 100,
                                               vm_coins.vm_coin_2 = 100,
                                               vm_coins.vm_coin_5 = 100,
                                               vm_coins.vm_coin_10 = 100
                                               WHERE vm_coins.id=1';
        $query_items_inside_code_1 = 'UPDATE vm_items SET vm_items.qnt = 10 WHERE vm_items.code=1005001';
        $query_items_inside_code_2 = 'UPDATE vm_items SET vm_items.qnt = 20 WHERE vm_items.code=1005002';
        $query_items_inside_code_3 = 'UPDATE vm_items SET vm_items.qnt = 20 WHERE vm_items.code=1005003';
        $query_items_inside_code_4 = 'UPDATE vm_items SET vm_items.qnt = 15 WHERE vm_items.code=1005004';
        $query_user_session = 'UPDATE users_session SET users_session.money_inside_vm = 0 WHERE users_session.user_id=1';
        $query_truncate_user_items = 'TRUNCATE TABLE user_items';
        $this->query('set autocommit=0');
        $this->query('Start transaction');
        $this->query($query_users);
        $this->query($query_vm_coins);
        $this->query($query_user_session);
        $this->query($query_truncate_user_items);
        $this->query($query_items_inside_code_1);
        $this->query($query_items_inside_code_2);
        $this->query($query_items_inside_code_3);
        $this->query($query_items_inside_code_4);
        $this->query('commit');
        return 'success';

    }




    //Забираем сдачу
    public function take_change()
    {
        //Узнаем, сколько внесено денег
        $res = $this->getArray('users_session', 'WHERE users_session.user_id=1');
        $money_inside = $res[0]['money_inside_vm'];
        $change_arr = ['coin_1' => 0, 'coin_2' => 0,  'coin_5' => 0,  'coin_10' => 0 ];

        //Узнаем, сколько монет и какого достоинства необходимо для того, чтобы вернуть пользователю деньги
        $change_arr['coin_10'] = intdiv($money_inside, 10);
        $rest = $money_inside % 10;
        if ($rest > 0)
        {
            $change_arr['coin_5'] = intdiv($rest, 5);
            $rest = $rest % 5;
        }
        if ($rest > 0)
        {
            $change_arr['coin_2'] = intdiv($rest, 2);
            $rest = $rest % 2;
        }
        if ($rest > 0)
        {
            $change_arr['coin_1'] = intdiv($rest, 1);
        }

        $query_users = 'UPDATE users SET users.user_coin_1=users.user_coin_1+'.$change_arr["coin_1"].',
                                         users.user_coin_2=users.user_coin_2+'.$change_arr["coin_2"].',
                                         users.user_coin_5=users.user_coin_5+'.$change_arr["coin_5"].',
                                         users.user_coin_10=users.user_coin_10+'.$change_arr["coin_10"].'
                                         WHERE users.id=1';
        $query_vm_coins = 'UPDATE vm_coins SET vm_coins.vm_coin_1 = vm_coins.vm_coin_1-'.$change_arr["coin_1"].',
                                               vm_coins.vm_coin_2 = vm_coins.vm_coin_2-'.$change_arr["coin_2"].',
                                               vm_coins.vm_coin_5 = vm_coins.vm_coin_5-'.$change_arr["coin_5"].',
                                               vm_coins.vm_coin_10 = vm_coins.vm_coin_10-'.$change_arr["coin_10"].'
                                               WHERE vm_coins.id=1';
        $query_user_session = 'UPDATE users_session SET users_session.money_inside_vm = users_session.money_inside_vm-'.$money_inside.' WHERE users_session.user_id=1';
        $this->query('set autocommit=0');
        $this->query('Start transaction');
        $this->query($query_users);
        $this->query($query_vm_coins);
        $this->query($query_user_session);
        $this->query('commit');
        return 'success';
    }







}