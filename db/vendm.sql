-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 21 2018 г., 01:18
-- Версия сервера: 10.2.11-MariaDB
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `vendm`
--

-- --------------------------------------------------------

--
-- Структура таблицы `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `code` int(11) NOT NULL COMMENT 'Код товара',
  `caption` varchar(255) NOT NULL COMMENT 'Название товара'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Товар, который может быть помещен в автомат';

--
-- Дамп данных таблицы `items`
--

INSERT INTO `items` (`id`, `code`, `caption`) VALUES
(1, 1005001, 'Чай'),
(2, 1005002, 'Кофе'),
(3, 1005003, 'Кофе с молоком'),
(4, 1005004, 'Сок');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `user_coin_1` int(11) NOT NULL COMMENT 'Кол-во монет 1 руб',
  `user_coin_2` int(11) NOT NULL COMMENT 'Кол-во монет 2 руб',
  `user_coin_5` int(11) NOT NULL COMMENT 'Кол-во монет 5 руб',
  `user_coin_10` int(11) NOT NULL COMMENT 'Кол-во монет 10 руб'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Кошелек пользователя';

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `user_coin_1`, `user_coin_2`, `user_coin_5`, `user_coin_10`) VALUES
(1, 10, 30, 20, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `users_session`
--

CREATE TABLE `users_session` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `user_id` int(11) NOT NULL COMMENT 'ID пользователя, который взаимодействует с автоматом',
  `money_inside_vm` int(11) NOT NULL COMMENT 'Та сумма, которую пользователь УЖЕ внес в автомат'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Сиссия взаимодействия пользователь-автомат';

--
-- Дамп данных таблицы `users_session`
--

INSERT INTO `users_session` (`id`, `user_id`, `money_inside_vm`) VALUES
(1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user_items`
--

CREATE TABLE `user_items` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `user_id` int(11) NOT NULL COMMENT 'ID пользователя, которому принадлежат купленные напитки',
  `code` int(11) NOT NULL COMMENT 'code из таблицы VM-items'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Купленные пользователем позиции';

-- --------------------------------------------------------

--
-- Структура таблицы `vm_coins`
--

CREATE TABLE `vm_coins` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `vm_coin_1` int(11) NOT NULL COMMENT 'Кол-во монет 1 руб',
  `vm_coin_2` int(11) NOT NULL COMMENT 'Кол-во монет 2 руб',
  `vm_coin_5` int(11) NOT NULL COMMENT 'Кол-во монет 5 руб',
  `vm_coin_10` int(11) NOT NULL COMMENT 'Кол-во монет 10 руб'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Кошелек VM';

--
-- Дамп данных таблицы `vm_coins`
--

INSERT INTO `vm_coins` (`id`, `vm_coin_1`, `vm_coin_2`, `vm_coin_5`, `vm_coin_10`) VALUES
(1, 100, 100, 100, 100);

-- --------------------------------------------------------

--
-- Структура таблицы `vm_items`
--

CREATE TABLE `vm_items` (
  `id` int(11) NOT NULL COMMENT 'Ключ',
  `code` int(11) NOT NULL COMMENT 'Код товара',
  `qnt` int(11) NOT NULL COMMENT 'Количество товара в вендинговом автомате',
  `price` int(11) NOT NULL COMMENT 'Стоимость'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Ассортимент товаров для продажи';

--
-- Дамп данных таблицы `vm_items`
--

INSERT INTO `vm_items` (`id`, `code`, `qnt`, `price`) VALUES
(1, 1005001, 10, 13),
(2, 1005002, 20, 18),
(3, 1005003, 20, 21),
(4, 1005004, 15, 35);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users_session`
--
ALTER TABLE `users_session`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `user_items`
--
ALTER TABLE `user_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `code` (`code`);

--
-- Индексы таблицы `vm_coins`
--
ALTER TABLE `vm_coins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `vm_items`
--
ALTER TABLE `vm_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`code`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users_session`
--
ALTER TABLE `users_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `user_items`
--
ALTER TABLE `user_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ';

--
-- AUTO_INCREMENT для таблицы `vm_coins`
--
ALTER TABLE `vm_coins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `vm_items`
--
ALTER TABLE `vm_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Ключ', AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `users_session`
--
ALTER TABLE `users_session`
  ADD CONSTRAINT `users_session_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `user_items`
--
ALTER TABLE `user_items`
  ADD CONSTRAINT `user_items_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_items_ibfk_2` FOREIGN KEY (`code`) REFERENCES `items` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `vm_items`
--
ALTER TABLE `vm_items`
  ADD CONSTRAINT `vm_items_ibfk_1` FOREIGN KEY (`code`) REFERENCES `items` (`code`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
