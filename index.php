<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

    <title>Vending machine - Автомат по продаже напитков</title>
</head>
<body>

<div class="container">
    <h1 class="display-3">Автомат по продаже напитков</h1>
</div>
<div class="container">
    <div class="row align-items-start">
        <div class="col-sm-4">

            <p class="lead">
                Кошелек пользователя
            </p>

            <div class="alert alert-primary" role="alert">
                1 руб - <span id="user_coin_1">-</span> шт.
                <button onclick="PutCointToVm(1)" type="button" class="btn btn-primary btn-sm" title="Внести монету 1 руб."> >> </button>
            </div>
            <div class="alert alert-primary" role="alert">
                2 руб - <span id="user_coin_2">-</span> шт.
                <button onclick="PutCointToVm(2)" type="button" class="btn btn-primary btn-sm" title="Внести монету 2 руб."> >> </button>
            </div>
            <div class="alert alert-primary" role="alert">
                5 руб - <span id="user_coin_5">-</span> шт.
                <button onclick="PutCointToVm(5)" type="button" class="btn btn-primary btn-sm" title="Внести монету 5 руб."> >> </button>
            </div>
            <div class="alert alert-primary" role="alert">
                10 руб - <span id="user_coin_10">-</span> шт.
                <button onclick="PutCointToVm(10)" type="button" class="btn btn-primary btn-sm" title="Внести монету 10 руб."> >> </button>
            </div>

        </div>
        <div class="col-sm-5">
            <p class="lead">
                Ассортимент товаров
            </p>

            <span id="vm_items"></span>


        </div>
        <div class="col-sm-3">
            <p class="lead">
                Кошелек VM
            </p>

            <div class="alert alert-success" role="alert">
                1 руб. - <span id="vm_coin_1">-</span> шт.
            </div>

            <div class="alert alert-success" role="alert">
                2 руб. - <span id="vm_coin_2">-</span> шт.
            </div>

            <div class="alert alert-success" role="alert">
                5 руб. - <span id="vm_coin_5">-</span> шт.
            </div>

            <div class="alert alert-success" role="alert">
                10 руб. - <span id="vm_coin_10">-</span> шт.
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-4">
            <p class="lead">
                Купленный товар:
            </p>
            <span id="user_items">---</span>

        </div>
        <div class="col-sm-5">
            <p class="lead">
                Внесенная сумма, руб:
            </p>
            <div class="alert alert-warning" role="alert">
                <h1 id="money_inside_vm" class="display-4">-</h1>
            </div>
        </div>
        <div class="col-sm-3">
            <p class="lead">
                <button type="button" onclick="TakeChange()" class="btn btn-success">Сдача</button>
            </p>
            <p class="lead">
                <button type="button" onclick="Reset()" class="btn btn-warning">Сброс сессии</button>
            </p>
        </div>
    </div>
</div>
<div class="container">
    <div class="row align-items-end">
        <div class="col-sm">
            (c) Ivan Saldikov 2018
        </div>
    </div>
</div>





<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<div id="id" style="visibility: hidden;"></div>


<script language="javascript">
    <!--


    //======================================
    //Формируем JSON-объект
    //======================================
    var JSONOjbValues = {
        "id": 0,
        "user_coin_1": "0",
        "user_coin_2": "0",
        "user_coin_5": "0",
        "user_coin_10": "0",
        "vm_coin_1": "0",
        "vm_coin_2": "0",
        "vm_coin_5": "0",
        "vm_coin_10": "0",
        "money_inside_vm": "0",
        "user_items": "---",
        "vm_items": []
    };


    //======================================
    // по окончании загрузки страницы
    //======================================
    $(document).ready(function(){
        RequestData();
    });



    //======================================
    //Обновляем вид приложения (получая обновленные данные из БД)
    //======================================
    function UpdateView() {
        RequestData();
    }



    //======================================
    //Сбрасываем мсессию
    //======================================
    function Reset() {
        $.ajax({
            url: 'request.php?action=reset',             // указываем URL и
            dataType: "json",                     // тип загружаемых данных
            success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                //console.log(data);
                $.each(data, function (i, val) {    // обрабатываем полученные данные
                    //console.log(i + " => " + val);
                    if (val === 'success')
                    {
                        alert('Данные успешно сброшены на первоначальные значения');
                    }
                });
            },
            complete: function(){
                //Обновляем вид
                UpdateView();
            }
        });

    }



    //======================================
    //Возвращаем сдачу
    //======================================
    function TakeChange() {
        $.ajax({
            url: 'request.php?action=take_change',             // указываем URL и
            dataType: "json",                     // тип загружаемых данных
            success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                //console.log(data);
                $.each(data, function (i, val) {    // обрабатываем полученные данные
                    //console.log(i + " => " + val);
                });
            },
            complete: function(){
                //Обновляем вид
                UpdateView();
            }
        });

    }




    //======================================
    //Кладем монету в автомат
    //======================================
    function Buy(code) {
        $.ajax({
            url: 'request.php?action=buy&code='+code,             // указываем URL и
            dataType: "json",                     // тип загружаемых данных
            success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                //console.log(data);
                $.each(data, function (i, val) {    // обрабатываем полученные данные
                    //console.log(i + " => " + val);
                    if (val === 'not_enough_money') {
                        alert('Недостаточно средств');
                    }
                    if (val === 'not_enough_items') {
                        alert('Выбранный товар закончился в автомате');
                    }
                });

            },
            complete: function(){
                //Обновляем вид
                UpdateView();
            }
        });

    }



    //======================================
    //Покупаем товар
    //======================================
    function PutCointToVm(coin) {

        $.ajax({
            url: 'request.php?action=put_coin&coin='+coin,             // указываем URL и
            dataType: "json",                     // тип загружаемых данных
            success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                //console.log(data);
                $.each(data, function (i, val) {    // обрабатываем полученные данные
                    //console.log(i + " => " + val);
                    if (val === 'not_enough_coins') {
                        alert('Монеты кончились!');
                    }
                });
            },
            complete: function(){
                //Обновляем вид
                UpdateView();
            }
        });

    }



    //======================================
    //Функция для вывода всего ассортимента товара
    //======================================
    function ShowVmItems() {
        var vm_items = JSONOjbValues["vm_items"];
        var vm_items_echo_total = '';
        $.each(vm_items, function (i, val) {    // обрабатываем полученные данные
            //console.log(i+' => '+val);
            vm_items_echo_total += ViewOneVmItem(val["name"], val["price"], val["code"], val["qnt"])
        });
        document.getElementById("vm_items").innerHTML = vm_items_echo_total;
    }
    /**
     * Вспомогательная функция для вывода одного пункта товара
     * @return {string}
     */
    function ViewOneVmItem(name, price, code, qnt) {
        var vm_items_echo = '';
        vm_items_echo += '<div class="alert alert-secondary" role="alert">';
        vm_items_echo += '<button onclick="Buy('+code+')" type="button" class="btn btn-secondary btn-sm" title="Получить товар, одну порцию: '+name+'"> << </button>';
        vm_items_echo += ' ' +name+' - ' + price +' руб.,';
        vm_items_echo += ' <span id="qnt_'+code+'">'+qnt+'</span> порций</div>';
        return vm_items_echo;
    }


    //======================================
    //Функция для вывода купленного пользователем товара
    //======================================
    function ShowUserItems() {
        var user_items = JSONOjbValues["user_items"];
        var user_items_echo_total = '';
        $.each(user_items, function (i, val) {    // обрабатываем полученные данные
            //console.log(i);
            user_items_echo_total += '<div class="alert alert-secondary" role="alert">' + val + '</div>';

        });
        if (user_items_echo_total === '') {
            user_items_echo_total = '---';
        }
        document.getElementById("user_items").innerHTML = user_items_echo_total;
    }




    //======================================
    //Получаем исходные данные и заполняем поля на форме
    //======================================
    function RequestData() {
        $.ajax({
            url: 'request.php?action=get_initial_data',             // указываем URL и
            dataType: "json",                     // тип загружаемых данных
            success: function (data, textStatus) { // вешаем свой обработчик на функцию success
                $.each(data, function (i, val) {    // обрабатываем полученные данные
                    //console.log(i + " => " + val);
                    JSONOjbValues[i] = val; //Назначаем значения для всех исходных данных
                });
                //console.log(JSONOjbValues);
                //Обновляем вид
                FillData();
                ShowVmItems();
                ShowUserItems();
            }
        });
    }


    //======================================
    //Заполняем данными DOM-элементы
    //======================================
    function FillData() {
        for (key in JSONOjbValues)	{
            if (document.getElementById(key) !== null) {
                document.getElementById(key).innerHTML = JSONOjbValues[key]; //key == ID DOM-объекта в HTML
            }
        }
    }


    //-->
</script>



</body>
</html>